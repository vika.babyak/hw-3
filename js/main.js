show("<b>Задание:</b><br>Создайте массив styles с элементами «Джаз» и «Блюз».<br>Добавьте «Рок-н-ролл» в конец.<br>Замените значение в середине на «Классика». Ваш код для поиска значения в середине должен работать для массивов с любой длиной.<br>Удалите первый элемент массива и покажите его.<br>Вставьте «Рэп» и «Регги» в начало массива.<br>");

show("<b>Результат:</b>");

// array
const styles = ["Джаз", "Блюз"];
show(`Начальный массив: <b>${styles}</b>`);

// adding an element to the end
styles.push("Рок-н-ролл");
show(`Массив после добавления элемента в конец: <b>${styles}</b>`);

// replacing an element in the middle
styles.splice(Math.floor(styles.length / 2), 1, "Классика");
show(`Массив после замены элемента в середине: <b>${styles}</b>`);

// removing the first element
show(`Удалённый элемент: <b>${styles.shift()}</b>`);

// adding elements to the start
styles.unshift("Рэп", "Регги");
show(`Массив после добавления элементов в начало: <b>${styles}</b>`);

// show function
function show(showElement) {
    document.write(showElement + "<br>");
}


